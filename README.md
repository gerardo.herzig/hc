# HC

Automated Health Check / OnGres

### Requirements

This code should be executed on a host  with any tipical linux distro (RedHat, Ubuntu, etc) installed and:
- python 3 installed
- Python virtual env support:
``` sudo   apt-get install python3-venv ``` (on Debian like distros)
- ansible support (version 2.9 or higher). Check out [this site](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) for installing Ansible.
- Access to the database hosts via ssh service (usually port 22) 
- Access to the PostgreSQL service (usually port 5432)

On target host (the database servers) you will need install

- psycopg2
- libpg-dev (Debian) libpgxx-devel (RHEL)




### How to prepare and run IHC Ansible Playbook


- Clone this repo (if you havent already):
git clone https://gitlab.com/ongresportal/hc.git

- Install dependecies 
```
cd ansible 
```

```
make setup
```

- Edit the file under `$PWD/playbooks/file.vault` and set the value of `pgpass` variable according to the password used for the selected postgreSQL supreuser

- Edit the file under `$PWD/inventories/group_vars/all.yml` and set the `db_server` var accordingly

- Execute the HC

```
make run USR=[user who connect to postgresql and ssh connect] TARGET=db_server

```


Results will be uploaded automatically for further analysis
