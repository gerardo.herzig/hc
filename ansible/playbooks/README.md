## Future implementation for collection stats from ansible playbooks

We might take a look on https://github.com/fboender/ansible-cmdb 


### Inventory

One per customer. Encrypt with vault.

### Roles

- OS target/Host (bare metal server)
- Postgres instance SQL interface/catalog
- Docker containers against a docker-compose setup or docker straight
- VMs?

### Flow

- Create a compressed folder, stream to a local dir.

### Output format

We try to keep the folder format compatible with `collect-host-stats.sh`.

